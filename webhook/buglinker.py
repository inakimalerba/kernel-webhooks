"""Update bugs to contain links to an MR and successful pipeline artifacts."""
from configparser import ConfigParser
import re
import sys

from bugzilla import BugzillaError
from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from gitlab.exceptions import GitlabGetError

from . import common

LOGGER = logger.get_logger('cki.webhook.buglinker')
SESSION = session.get_session('cki.webhook.buglinker')

KERNEL_BZ_BOT = 'cki-ci-bot+kernel-workflow-bugzilla@redhat.com'


def get_bugs(bzcon, bug_list):
    """Return a list of bug objects."""
    try:
        bz_results = bzcon.getbugs(bug_list)
        if not bz_results:
            LOGGER.info("getbugs() returned an empty list for these bugs: %s.", bug_list)
        return bz_results
    except BugzillaError:
        LOGGER.exception('Error getting bugs.')
        return None


def update_bugzilla(project, bugs, mr_id, bzcon):
    """Filter input bug list and run bugzilla API actions."""
    if not bugs:
        LOGGER.debug('Input bug list is empty.')
        return
    # Ignore non-numeric bugs such as 'INTERNAL'. Bug list items are strings :/.
    bugs_to_exclude = list(filter(lambda bug: not bug.isdigit(), bugs))
    if bugs_to_exclude:
        LOGGER.info('Excluding these "bugs" from bugzilla actions: %s', bugs_to_exclude)
        if bugs_to_exclude == bugs:
            return

    bugs_filtered = [item for item in bugs if item not in bugs_to_exclude]
    if bugs_filtered:
        bugs = get_bugs(bzcon, bugs_filtered)
        add_mr_to_bz(bzcon, project, bugs, mr_id)


def bz_is_linked_to_mr(bug, domain, path):
    """Return True if any of the bug's trackers point to the given MR."""
    if not bug.external_bugs:
        return False
    for tracker in bug.external_bugs:
        if tracker['type']['description'] != "Gitlab":
            continue
        if tracker['type']['url'].split('/')[2] == domain and \
           tracker['ext_bz_bug_id'] == path:
            return True
    return False


def get_mr_domain_path(project, mr_id):
    """Return properties needed for external tracker work."""
    mr_domain = project.web_url.split('/')[2]
    mr_path = f"{project.path_with_namespace}/-/merge_requests/{mr_id}"
    return (mr_domain, mr_path)


def _do_add_mr_to_bz(bzcon, bug_list, mr_domain, mr_path):
    if misc.is_production():
        ext_domain = f"https://{mr_domain}/"
        try:
            bzcon.add_external_tracker(bug_list, ext_type_url=ext_domain, ext_bz_bug_id=mr_path)
        except BugzillaError:
            LOGGER.exception("Problem adding tracker %s%s to BZs.", ext_domain, mr_path)


def add_mr_to_bz(bzcon, project, bugs, mr_id):
    """Take a list of bugs and a MR# and update the BZ's ET with the MR url."""
    # Properties that we want to match when checking a BZ's trackers
    (mr_domain, mr_path) = get_mr_domain_path(project, mr_id)

    untracked_bugs = list(filter(lambda bug: not bz_is_linked_to_mr(bug, mr_domain, mr_path),
                                 bugs))
    if not untracked_bugs:
        LOGGER.info("All bugs have an existing link to MR %d.", mr_id)
        return

    new_bugs = list(map(lambda bug: bug.id, untracked_bugs))
    LOGGER.info("Need to add MR %d to external tracker list of these bugs: %s", mr_id, new_bugs)
    _do_add_mr_to_bz(bzcon, new_bugs, mr_domain, mr_path)


BASIC_BZ_FIELDS = ['component',
                   'external_bugs',
                   'id',
                   'summary'
                   ]

BASIC_BZ_QUERY = {'query_format': 'advanced',
                  'include_fields': BASIC_BZ_FIELDS,
                  'classification': 'Red Hat',
                  'component': 'kernel-rt'
                  }


def parse_cve_from_summary(summary):
    """Return a CVE ID from the string, or None."""
    result = re.match(r'^(?P<cve>CVE-\d{4}-\d{4,5})\s', summary)
    if result:
        return result.group('cve')
    return None


def get_rt_cve_bugs(bzcon, bug_list):
    """Identify whether bug_list BZs are CVEs and if so return matching kernel-rt bugs."""
    bz_results = get_bugs(bzcon, bug_list)
    cve_set = set()
    for bug in bz_results:
        cve_id = parse_cve_from_summary(bug.summary)
        if not cve_id:
            continue
        cve_set.add(cve_id)

    if not cve_set:
        return None

    cve_query = {**BASIC_BZ_QUERY}
    cve_query['product'] = bz_results[0].product
    # Blocked by the CVE
    cve_query['f1'] = 'blocked'
    cve_query['o1'] = 'anywords'
    cve_query['v1'] = list(cve_set)
    # Is not CLOSED DUPLICATE.
    cve_query['f2'] = 'resolution'
    cve_query['o2'] = 'notequals'
    cve_query['v2'] = 'DUPLICATE'
    # Is the same version.
    cve_query['f3'] = 'version'
    cve_query['o3'] = 'equals'
    cve_query['v3'] = bz_results[0].version

    try:
        return bzcon.query(cve_query)
    except BugzillaError:
        LOGGER.exception('Error querying bugzilla.')
        return None


def post_to_bugs(bug_list, post_text, pipeline_url, bridge_name, bzcon):
    """Submit post to given bugs."""
    filtered_bugs = []
    # If this is the RT pipeline and a CVE then we want to post to the kernel-rt bug.
    if bridge_name == common.KERNEL_RT_PIPELINE:
        bugs = get_rt_cve_bugs(bzcon, bug_list)
    else:
        bugs = get_bugs(bzcon, bug_list)
    if not bugs:
        return None

    # It is possible for there to be multiple 'success' events for a given pipeline
    # so we should check that we haven't already posted them to the bugs. Sigh.
    for bug in bugs:
        if not comment_already_posted(bug, pipeline_url):
            filtered_bugs.append(bug.id)
    if not filtered_bugs:
        LOGGER.info('Pipeline results have already been posted to all relevant bugs.')
        return None

    if misc.is_production():
        try:
            comment = bzcon.build_update(comment=post_text, comment_private=True)
            bzcon.update_bugs(filtered_bugs, comment)
        except BugzillaError:
            LOGGER.exception('Error posting comments to bugs: %s', filtered_bugs)
            return None
    LOGGER.info('Posted comment to bugs: %s', filtered_bugs)
    return bugs


def comment_already_posted(bug, pipeline_url):
    """Return True if the pipeline results have already been posted to the given bug."""
    pipeline_text = f'Pipeline: {pipeline_url}'
    comments = bug.getcomments()
    for comment in comments:
        if comment['creator'] == KERNEL_BZ_BOT and pipeline_text in comment['text']:
            LOGGER.info('Excluding bug %s as pipeline was already posted in comment %s.', bug.id,
                        comment['count'])
            return True
    return False


def create_bugzilla_text(title, mr_url, pipeline_url, repo_url, job_data):
    """Return formatted job data."""
    bz_post = 'The following Merge Request has pipeline job artifacts available:\n\n'
    bz_post += f'Title: {title}\nMR: {mr_url}\n'
    bz_post += f'Pipeline: {pipeline_url}\n\n'
    bz_post += ('This Repo URL is *not* accessible from a web browser!'
                ' It only functions as a dnf or yum baseurl.\n')

    bz_post += f'Repo URL: {repo_url}\n\n'

    for job in job_data:
        bz_post += '\n'
        bz_post += f"{job['version']}.{job['arch']}:\n"
        bz_post += f"Gitlab browser: {job['browse_url']}\n"
        # Include test job details if we have them.
        if job['status']:
            bz_post += f"Job: {job['job_url']}\n"
            bz_post += f"Current automated test status: {job['status']}\n"
    return bz_post


def get_artifact(job, path):
    """Return the given file from the job."""
    try:
        return job.artifact(path)
    except GitlabGetError:
        LOGGER.warning('Error getting path for job #%d: %s', job.id, path)
        return None


def parse_job_rc(job):
    """Return a dict with the arch, kernel version, and URLs from a job rc file."""
    # If the test job was cancled there will be no artifacts so no rc file to parse.
    if job.status == 'canceled':
        arch = job.name.split()[-1]
        version = None
        browse_url = None
        repo_url = None
    else:
        rc_file = get_artifact(job, 'rc')
        if not rc_file:
            return {}

        rc_config = ConfigParser()
        rc_config.read_string(rc_file.decode())

        arch = rc_config['state'].get('kernel_arch')
        version = rc_config['state'].get('kernel_version')
        browse_url = rc_config['state'].get('kernel_browse_url')
        repo_url = rc_config['state'].get('kernel_package_url').replace(arch, '$basearch')

    # The job status is set to None for non-test jobs (publish jobs)
    job_data = {'id': job.id,
                'arch': arch,
                'version': version,
                'browse_url': browse_url,
                'repo_url': repo_url,
                'job_url': job.web_url,
                'status': job.status if job.stage == 'test' else None
                }

    return job_data


def get_pipeline_job_ids(pipeline, bridge_job):
    """Return the list of job IDs depending on the bridge_job name."""
    job_ids = {}
    jobs = pipeline.jobs.list(as_list=False)
    # The RT pipeline does not have test jobs so instead return its publish jobs.
    if bridge_job == common.KERNEL_PIPELINE:
        job_ids = [job.id for job in jobs if job.stage == 'test']
    if bridge_job == common.KERNEL_RT_PIPELINE:
        job_ids = [job.id for job in jobs if job.stage == 'publish' and job.status == 'success']
    return job_ids


def bugs_in_mr_description(description):
    """Return the list of all the Bugzilla: bugs mentioned in an MR description."""
    if not description:
        return []
    bugs = set([])
    for line in description.splitlines():
        bug = common.find_bz_in_line(line, 'Bugzilla')
        if bug:
            bugs.add(bug)
    return list(bugs)


def mr_event_should_run_pipeline(msg):
    """Is true if MR has readyForQA & event shows WIP status removed or readyForQA label added."""
    if 'title' in msg.payload['changes']:
        old_title = msg.payload['changes']['title'].get('previous', '')
        new_title = msg.payload['changes']['title'].get('current', '')

        str_tuple = ('[Draft]', 'Draft:', '(Draft)', '[WIP]', 'WIP:')
        if old_title and (old_title.startswith(str_tuple) and not new_title.startswith(str_tuple)):
            return True
    return False


def process_pipeline(gl_instance, msg):
    """Parse pipeline or merge_request msg and run _process_pipeline."""
    if msg.payload['object_kind'] == 'pipeline':
        mr_id = msg.payload["merge_request"]["iid"]
    elif msg.payload['object_kind'] == 'merge_request':
        mr_id = msg.payload["object_attributes"]["iid"]

    project = gl_instance.projects.get(msg.payload["project"]["id"])
    merge_request = project.mergerequests.get(mr_id)
    if merge_request.work_in_progress:
        LOGGER.info("MR %s is marked work in progress, ignoring.", mr_id)
        return
    if merge_request.head_pipeline is None:
        LOGGER.info("MR %s has not triggered any pipelines? head_pipeline is None.", mr_id)
        return

    pipeline_id = merge_request.head_pipeline.get('id')
    if not merge_request.head_pipeline.get('web_url', '').startswith('https://gitlab.com/redhat/'):
        LOGGER.info("MR %s head pipeline #%s is not in the Red Hat namespace: %s", mr_id,
                    pipeline_id, merge_request.head_pipeline.get('web_url'))
        return

    _process_pipeline(gl_instance, project, pipeline_id, merge_request)


def pipeline_finished(status):
    """Return True if the pipeline is finished."""
    return status in ('success', 'failed')


def process_bridge_job(gl_instance, downstream_project, bridge_job):
    # pylint: disable=too-many-return-statements
    """Process the bridge job and return jobs_data."""
    if bridge_job.name not in (common.KERNEL_PIPELINE, common.KERNEL_RT_PIPELINE):
        LOGGER.info('Unrecognized bridge job: %s', bridge_job.name)
        return None

    LOGGER.info('Processing bridge job: %s', bridge_job.name)

    # Possibly the bridge job does not have a downstream pipeline yet.
    if not bridge_job.downstream_pipeline:
        LOGGER.info('Bridge job %s does not have a downstream pipeline.', bridge_job.name)
        return None

    # Assume the downstream project is the same for all bridge jobs...
    if not downstream_project:
        downstream_namespace = \
            common.parse_gl_project_path(bridge_job.downstream_pipeline['web_url'])
        downstream_project = gl_instance.projects.get(downstream_namespace)

    downstream_pipeline = downstream_project.pipelines.get(bridge_job.downstream_pipeline.get('id'))
    if not downstream_project or not downstream_pipeline:
        LOGGER.info('Could not get downstream pipeline for bridge job %s.', bridge_job.name)
        return None

    # If the pipeline isn't finished give up and wait for next time.
    if not pipeline_finished(downstream_pipeline.status):
        LOGGER.info('Pipeline %s is not finished: %s', downstream_pipeline.id,
                    downstream_pipeline.status)
        return None

    # Get the test job IDs or, for KERNEL_RT_PIPELINE, the *succesful* publish job IDs.
    job_ids = get_pipeline_job_ids(downstream_pipeline, bridge_job.name)
    if not job_ids:
        LOGGER.info('No successful publish jobs found for downstream pipeline %s in project %s.',
                    downstream_pipeline.id, downstream_project.name)
        return None

    # Process each job and for each one store necessary facts in a dict.
    jobs_data = []
    for job_id in job_ids:
        # "Job methods (play, cancel, and so on) are not available on ProjectPipelineJob objects.
        # To use these methods create a ProjectJob object."
        job_data = parse_job_rc(downstream_project.jobs.get(job_id))
        if job_data:
            jobs_data.append(job_data)

    # If we don't have any artifacts or all the jobs were canceled then we have nothing
    # useful to post.
    if not [job for job in jobs_data if job['status'] != 'canceled']:
        LOGGER.info('No artifact data; nothing to post.')
        return None

    return jobs_data


def _process_pipeline(gl_instance, project, pipeline_id, merge_request):
    """Process a successful pipeline event message."""
    # If the MR doesn't have any bugs listed, then we have nothing to do.
    bug_list = bugs_in_mr_description(merge_request.description)
    if not bug_list:
        LOGGER.info('No bugs found in MR %s description.', merge_request.iid)
        return

    local_pipeline = project.pipelines.get(pipeline_id)
    downstream_project = None
    bzcon = None

    # Process each of the expected bridge jobs separately.
    for bridge_job in local_pipeline.bridges.list():
        jobs_data = process_bridge_job(gl_instance, downstream_project, bridge_job)
        if not jobs_data:
            LOGGER.debug('No job data.')
            continue

        # We need a bugzilla connection to be able to just post.
        if not bzcon:
            bzcon = common.try_bugzilla_conn()
            if not bzcon:
                LOGGER.error('No bugzilla connection.')
                continue

        pipeline_url = f'https://gitlab.com/{project.path_with_namespace}/-/pipelines/{pipeline_id}'
        repo_url = next(job['repo_url'] for job in jobs_data if job['repo_url'])
        bug_post_text = create_bugzilla_text(merge_request.title, merge_request.web_url,
                                             pipeline_url, repo_url, jobs_data)
        LOGGER.info('Generated comment text:\n%s', bug_post_text)
        bugs = post_to_bugs(bug_list, bug_post_text, pipeline_url, bridge_job.name, bzcon)

        # For kernel-rt we separately add external tracker links to those CVE BZs.
        if bridge_job.name == common.KERNEL_RT_PIPELINE and bugs:
            add_mr_to_bz(bzcon, project, bugs, merge_request.iid)


def process_mr(gl_instance, msg):
    """Process a merge request event message."""
    # Has the WIP flag been removed? Then run the pipeline linker.
    if mr_event_should_run_pipeline(msg):
        LOGGER.info('Sending MR event to pipeline processor.')
        process_pipeline(gl_instance, msg)

    # If the event is not for a new MR or does not correspond to some change in the MR
    # description then we can ignore it.
    if msg.payload["object_attributes"].get('action') != 'open' and \
            'description' not in msg.payload['changes']:
        LOGGER.info('MR has not changed, ignoring event.')
        return

    project = gl_instance.projects.get(msg.payload["project"]["id"])
    mr_id = msg.payload["object_attributes"]["iid"]
    gl_mergerequest = project.mergerequests.get(mr_id)
    mr_description = gl_mergerequest.description

    bugs = bugs_in_mr_description(mr_description)
    if not bugs:
        LOGGER.info('No bugs found in MR %s description.', mr_id)
        return

    bzcon = common.try_bugzilla_conn()
    if not bzcon:
        return

    update_bugzilla(project, bugs, mr_id, bzcon)


WEBHOOKS = {
    "merge_request": process_mr,
    'pipeline': process_pipeline,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGLINKER')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, ignore_msgs_from_self=args.dont_ignore_self)


if __name__ == "__main__":
    main(sys.argv[1:])
