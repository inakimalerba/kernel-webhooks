"""Cancel beaker jobs for finished GitLab jobs."""
import json
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import parse_gitlab_url
import gitlab

from . import common

LOGGER = logger.get_logger('cki.webhook.beaker_reaper')


class Reaper:
    """Cancel beaker jobs for finished GitLab jobs."""

    def __init__(self, parsed_args):
        """Construct a new instance."""
        self.parsed_args = parsed_args
        self.beaker_url = parsed_args.beaker_url
        self.bkr_binary = parsed_args.bkr_binary
        self.irc_bot_exchange = parsed_args.irc_bot_exchange
        if self.irc_bot_exchange:
            self.irc_messagequeue = common.get_messagequeue(parsed_args, keepalive_s=5)
        else:
            self.irc_messagequeue = None
        if parsed_args.beaker_owner:
            self.owner_filter_args = ['--owner', parsed_args.beaker_owner]
        else:
            self.owner_filter_args = []
        self.running_jobs = {}

    def run(self):
        """Run the main loop."""
        common.generic_loop(self.parsed_args, {'pipeline': self.process_pipeline})

    def _bkr(self, args):
        return subprocess.run([self.bkr_binary] + args, check=True, capture_output=True).stdout

    def process_pipeline(self, gl_instance, msg):
        """Process a pipeline event."""
        pipeline_id = msg.payload['object_attributes']['id']
        gl_project = gl_instance.projects.get(msg.payload['project']['id'], lazy=True)
        gl_pipeline = gl_project.pipelines.get(pipeline_id)

        if gl_pipeline.status not in ('success', 'failed', 'canceled'):
            return

        gl_bridges = gl_pipeline.bridges.list(all=True)
        if gl_bridges:
            self.stop_child_pipelines(gl_instance, gl_pipeline, gl_bridges)
        else:
            self.stop_beaker_jobs(gl_pipeline)

    def stop_child_pipelines(self, gl_instance, gl_parent_pipeline, gl_bridges):
        """For a finished pipeline, make sure all child pipelines are stopped."""
        for gl_bridge in gl_bridges:
            child_pipeline = gl_bridge.downstream_pipeline
            if child_pipeline and child_pipeline['status'] not in ('success', 'failed', 'canceled'):
                gl_project = gl_instance.projects.get(child_pipeline['project_id'], lazy=True)
                gl_pipeline = gl_project.pipelines.get(child_pipeline['id'])
                self.cancel_child_pipeline(gl_pipeline, gl_parent_pipeline)

    def stop_beaker_jobs(self, gl_pipeline):
        """For a finished pipeline, make sure all Beaker jobs are stopped."""
        jobs = json.loads(self._bkr(['job-list',
                                     '--whiteboard', f'cki@gitlab:{gl_pipeline.id}',
                                     '--unfinished'] + self.owner_filter_args))
        for job in jobs:
            self.cancel_beaker_job(job, gl_pipeline)

    def load_running_jobs(self):
        """Retrieve information about running Beaker jobs."""
        print('Loading running Beaker jobs')
        jobs = json.loads(self._bkr(['job-list',
                                     '--whiteboard', 'cki@gitlab',
                                     '--unfinished'] + self.owner_filter_args))
        for job in jobs:
            print(f'  getting results for {job}')
            root = ET.fromstring(self._bkr(['job-results',
                                            job]))
            whiteboard = root.find('./whiteboard')
            if whiteboard is None:
                LOGGER.warning('No whiteboard found for Beaker job %s', job)
                continue
            match = re.search(r'cki@gitlab:(\d+)', whiteboard.text)
            if not match:
                LOGGER.warning('No pipeline ID found for Beaker job %s', job)
                continue
            self.running_jobs[job] = int(match.group(1))

    def process_pipeline_project(self, project_url):
        """Process a pipeline repo."""
        _, gl_project = parse_gitlab_url(project_url)

        failures = 0
        for job, pipeline_id in self.running_jobs.items():
            print(f'Trying to find owner of running Beaker job {job}')

            try:
                gl_pipeline = gl_project.pipelines.get(pipeline_id)
            except gitlab.exceptions.GitlabGetError:
                print(f'  pipeline {pipeline_id} not found')
                continue

            gl_pipeline = gl_project.pipelines.get(pipeline_id)
            if gl_pipeline.status not in ('success', 'failed', 'canceled'):
                print(f'  pipeline {pipeline_id} not finished')
                continue

            try:
                self.cancel_beaker_job(job, gl_pipeline, output='print')
            except subprocess.CalledProcessError:
                LOGGER.warning('Unable to cancel Beaker job %s', job)
                failures += 1
        return failures

    def cancel_beaker_job(self, job: str, gl_pipeline, output='log'):
        """Cancel a Beaker job."""
        status = 'Cancelling' if misc.is_production() else 'Detected'
        pipeline_url = misc.shorten_url(gl_pipeline.web_url)
        message = (f'{status} running Beaker job '
                   f'{job} of finished pipeline {gl_pipeline.id} - pipeline {pipeline_url}')
        if self.beaker_url:
            beaker_url = misc.shorten_url(job.replace('J:', self.beaker_url + '/'))
            message += f' Beaker {beaker_url}'
        if output == 'log':
            LOGGER.info('%s', message)
        if output == 'print':
            print('  ' + message)
        if self.irc_messagequeue:
            self.irc_messagequeue.send_message(
                {'message': f'👻 {message}'}, 'irc.message', self.irc_bot_exchange)
        if misc.is_production():
            self._bkr(['job-cancel', '--msg', message, job])

    def cancel_child_pipeline(self, gl_pipeline, gl_parent_pipeline, output='log'):
        """Cancel a GitLab child pipeline."""
        status = 'Cancelling' if misc.is_production() else 'Detected'
        pipeline_url = misc.shorten_url(gl_pipeline.web_url)
        parent_pipeline_url = misc.shorten_url(gl_parent_pipeline.web_url)
        message = (f'👻 {status} running child pipeline '
                   f'{gl_pipeline.id} of finished parent pipeline {gl_parent_pipeline.id} - '
                   f'child {pipeline_url} parent {parent_pipeline_url}')
        if output == 'log':
            LOGGER.info('%s', message)
        if output == 'print':
            print('  ' + message)
        if self.irc_messagequeue:
            self.irc_messagequeue.send_message(
                {'message': message}, 'irc.message', self.irc_bot_exchange)
        if misc.is_production():
            gl_pipeline.cancel()


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BEAKER_REAPER')
    parser.add_argument('--pipeline-project-url', action='append', default=[],
                        help='Process jobs for a given pipeline project only')
    parser.add_argument('--beaker-url', default=os.environ.get('BEAKER_URL'),
                        help='Beaker URL for linking Beaker jobs')
    parser.add_argument('--bkr-binary', default='bkr',
                        help='path to the bkr binary')
    parser.add_argument('--irc-bot-exchange', default=os.environ.get('IRC_BOT_EXCHANGE'),
                        help='AMQP exchange to send messages to the IRC bot')
    parser.add_argument('--beaker-owner', default=os.environ.get('BEAKER_OWNER'),
                        help='Owner filter to select Beaker jobs')
    parsed_args = parser.parse_args(args)
    reaper = Reaper(parsed_args)
    if parsed_args.pipeline_project_url:
        reaper.load_running_jobs()
        failures = 0
        for pipeline_project_url in parsed_args.pipeline_project_url:
            failures += reaper.process_pipeline_project(pipeline_project_url)
        if failures:
            LOGGER.error('Some jobs could not be cancelled')
            sys.exit(1)
    else:
        reaper.run()


if __name__ == "__main__":
    main(sys.argv[1:])
