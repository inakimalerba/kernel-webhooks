"""Process all of the ACKs/NACKs that are associated with a merge request."""
import re
import sys

from cki_lib import logger
from cki_lib import misc

from . import cdlib
from . import common

LOGGER = logger.get_logger('cki.webhook.ack_nack')
MIN_REVIEWERS = 2
MIN_ARK_CONFIG_REVIEWERS = 1

READY_LABEL = ('This merge request has been reviewed by Red Hat engineering and approved for'
               ' inclusion.')
NEEDS_REVIEW_LABEL = 'This merge request needs more reviews and acks from Red Hat engineering.'
NACKED_LABEL = 'This merge request has been explicitly Nacked by Red Hat engineering.'


def _create_label(label):
    if label.endswith('::OK'):
        label_color = common.READY_LABEL_COLOR
        label_description = READY_LABEL
    else:
        label_color = common.NEEDS_REVIEW_LABEL_COLOR
        if label.endswith('::NeedsReview'):
            label_description = NEEDS_REVIEW_LABEL
        elif label.endswith('::NACKed'):
            label_description = NACKED_LABEL

    return common.create_label_object(label, label_color, label_description)


def _save(gl_instance, gl_project, gl_mergerequest, create_gl_status_note, status,
          subsys_scoped_labels, message):
    # pylint: disable=too-many-arguments
    note = f'ACK/NACK Summary: {status} - {message}'
    LOGGER.info(note)

    labels = []
    labels.append(_create_label(f'Acks::{status}'))
    for subsystem_scoped_label in subsys_scoped_labels:
        labels.append(_create_label(subsystem_scoped_label))

    common.add_label_to_merge_request(gl_instance, gl_project, gl_mergerequest.iid, labels)

    if create_gl_status_note and misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        gl_mergerequest.notes.create({'body': note})


def _get_required_reviewers(changed_files, owners_parser, submitter_email):
    """Parse the owners.yaml file and return a set of email addresses and subsystem labels."""
    if not changed_files:
        return set([])

    all_reviewers = {}

    # A merge request can span multiple subsystems so get the reviewers for each subsystem.
    # Call owners parser individually for each file.
    for changed_file in changed_files:
        for entry in owners_parser.get_matching_entries([changed_file]):
            reviewers = set([])
            reviewers.update([x['email'] for x in entry.get_maintainers()])
            reviewers.update([x['email'] for x in entry.get_reviewers()])

            # Ensure that the merge request submitter doesn't show up in the reviewers list
            if submitter_email in reviewers:
                reviewers.remove(submitter_email)

            if not reviewers:
                continue

            if entry.get_subsystem_label() not in all_reviewers:
                all_reviewers[entry.get_subsystem_label()] = set([])

            all_reviewers[entry.get_subsystem_label()].update(reviewers)

    LOGGER.debug('Subsystem reviewers: %s', all_reviewers)

    ret = []
    for subsystem in all_reviewers:
        ret.append((all_reviewers[subsystem], subsystem))

    return ret


def get_ark_config_mr_ccs(merge_request):
    """Return a list of any CC email addresses from an ark project MR description."""
    cc_list = []
    if not merge_request.description:
        return cc_list
    mlines = merge_request.description.splitlines()
    for line in mlines:
        if line.startswith('Cc: ') and line.endswith('@redhat.com>'):
            cc_list.append(line.split()[-1].split('<')[1].split('>')[0])
    return cc_list


def get_min_reviewers(project_id, merge_request, files):
    """Return the min number of reviews for the project and for ARK try to include reviewer set."""
    # For ark kernel config changes, also return users in th MR's CC line.
    if project_id == common.ARK_PROJECT_ID and all(file.startswith('redhat/configs/')
                                                   for file in files):
        cc_reviewers = get_ark_config_mr_ccs(merge_request)
        if cc_reviewers:
            return (MIN_ARK_CONFIG_REVIEWERS, [(set(cc_reviewers), None)])

    return (MIN_REVIEWERS, None)


def _validate_note_submitter(message, note_email, username):
    """Parse the message to make sure people aren't forging acks/nacks via email bridge."""
    if username != common.EMAIL_BRIDGE_ACCOUNT:
        return True

    bridged_email = None
    for line in message.split('\n'):
        pattern = " commented via email:"
        if not line.endswith(pattern):
            continue

        bridged_email = line.split(pattern)[0].split(' ')[-1].replace('<', '').replace('>', '')
        break

    if bridged_email != note_email:
        LOGGER.warning("Rejecting apparent forged ack/nack attempt (%s != %s)",
                       bridged_email, note_email)
        return False

    return True


def _parse_tag(message):
    """Parse an individual message and look for any tags of interest."""
    tag_re = re.compile(r"^((Na|A)cked-by|Re(scind|voke)-(N|n)?(A|a)cked-by): ")
    for line in message.split('\n'):
        match = tag_re.match(line)
        if not match:
            continue

        tag = match.group(0).strip(": ")
        name_email = line.split(": ")[1].split(' <')
        if len(name_email) != 2:
            continue

        name = name_email[0]
        email = name_email[1].split('>')[0]

        return (tag, name, email)

    return (None, None, None)


def _tag_email_error_message(tag, submitter_email, note_email, public_email, username):
    # cki-bot / cki-kwf-bot can impersonate other users for the approve/unapprove button.
    # redhat-patchlab leaves messages on behalf of users via the email bridge.
    if username in common.BOT_ACCOUNTS:
        return None

    if tag in ('Acked-by', 'Rescind-acked-by', 'Revoke-acked-by') and submitter_email == note_email:
        return '%s cannot self-ack merge request' % (submitter_email)

    if not public_email:
        return (f"Ignoring '{tag} <{note_email}>' since user does not have a public "
                "email address on their GitLab profile. Click on your avatar at the top "
                "right, go to settings, and ensure that your redhat.com address is set to "
                "your public email.")

    if public_email != note_email:
        return (f"Ignoring '{tag} <{note_email}>' since this doesn't match the "
                f"user's public email address {public_email} on GitLab. You can change "
                "this by clicking on your avatar at the top right, go to settings, "
                "and ensure that your redhat.com address is set to your public email.")

    return None


def _process_acks_nacks(messages, last_commit_timestamp, submitter_email,
                        gl_instance, gl_project, gl_mergerequest, rhkernel_src):
    # pylint: disable=too-many-branches,too-many-locals,too-many-arguments
    """Process a list of messages and collect any ACKs/NACKS."""
    acks = set([])
    nacks = set([])
    code_changes_checked = False
    message_before_ccts = False
    code_changed_ts = cdlib.get_last_code_changed_timestamp(gl_mergerequest)
    for message_timestamp, message, public_email, username in messages:
        (tag, name, note_email) = _parse_tag(message)
        if not tag:
            continue

        if not _validate_note_submitter(message, note_email, username):
            continue

        if not code_changes_checked:
            code_changed = cdlib.mr_code_changed(gl_instance, gl_project,
                                                 gl_mergerequest, rhkernel_src)
            code_changes_checked = True
        if code_changed_ts is not None:
            message_before_ccts = message_timestamp < code_changed_ts
        if last_commit_timestamp and (code_changed or message_before_ccts):
            ack_valid = last_commit_timestamp < message_timestamp
            if not ack_valid:
                LOGGER.warning("Ignoring '%s: %s <%s>' since code was changed at %s after ACKs",
                               tag, name, note_email, last_commit_timestamp)
        else:
            ack_valid = True

        nack_valid = True
        tag_error = _tag_email_error_message(tag, submitter_email, note_email, public_email,
                                             username)
        if tag_error:
            LOGGER.warning(tag_error)
            nack_valid = False
            ack_valid = False

        LOGGER.debug("Processing '%s: %s <%s>' ts=%s email=%s ack_valid=%s, nack_valid=%s",
                     tag, name, note_email, message_timestamp, public_email, ack_valid, nack_valid)

        name_email = (name, note_email)
        if tag == 'Acked-by' and ack_valid:
            acks.add(name_email)
            if name_email in nacks:
                nacks.remove(name_email)
        elif tag in ('Rescind-acked-by', 'Revoke-acked-by',
                     'Rescind-Acked-by', 'Revoke-Acked-by') and ack_valid:
            if name_email in acks:
                acks.remove(name_email)
            else:
                LOGGER.warning('Cannot find ACK to revoke for %s', name_email)
        elif tag == 'Nacked-by' and nack_valid:
            nacks.add(name_email)
        elif tag in ('Rescind-nacked-by', 'Revoke-nacked-by',
                     'Rescind-Nacked-by', 'Revoke-Nacked-by') and nack_valid:
            if name_email in nacks:
                nacks.remove(name_email)
            else:
                LOGGER.warning('Cannot find NACK to revoke for %s', name_email)
        elif ack_valid:
            LOGGER.warning("Unhandled tag: %s (from %s <%s>)", tag, name_email[0], name_email[1])

    return (acks, nacks)


def _show_ack_nacks(ack_nacks):
    summary = [f'{ack_nack[0]} <{ack_nack[1]}>' for ack_nack in ack_nacks]
    summary.sort()
    return ", ".join(summary)


def _has_acknack(ack_nacks, subsystem_reviewers):
    for ack_nack in ack_nacks:
        if ack_nack[1] in subsystem_reviewers:
            return True

    return False


def _get_old_subsystems_from_labels(gl_mergerequest):
    subsys_list = []
    subsys_labels = []
    for label in gl_mergerequest.labels:
        if label.startswith('Acks::'):
            sslabel_parts = label.split("::")
            if len(sslabel_parts) == 3:
                subsys_list.append(sslabel_parts[1])
                subsys_labels.append(label)
    return (subsys_list, subsys_labels)


def _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels):
    stale_labels = []
    for subsystem in old_subsystems:
        if subsystem not in subsys_scoped_labels.keys():
            for label in old_labels:
                if label.startswith(f"Acks::{subsystem}::"):
                    stale_labels.append(label)
    LOGGER.debug("Stale labels: %s", stale_labels)
    return stale_labels


def _get_subsys_scoped_labels(gl_project, gl_mergerequest, acks, nacks, all_reviewers):
    # pylint: disable=too-many-locals,too-many-arguments
    subsys_scoped_labels = {}
    subsys_without_acks = set([])
    (old_subsystems, old_labels) = _get_old_subsystems_from_labels(gl_mergerequest)

    for subsystem_reviewers, subsystem_label in all_reviewers:
        subsystem_has_an_ack = _has_acknack(acks, subsystem_reviewers)
        subsystem_has_a_nack = _has_acknack(nacks, subsystem_reviewers)

        if subsystem_has_a_nack:
            scoped_label = 'NACKed'
        elif not subsystem_has_an_ack:
            subsys_without_acks.add(frozenset(subsystem_reviewers))
            scoped_label = 'NeedsReview'
        else:
            scoped_label = 'OK'

        if subsystem_label:
            subsys_scoped_labels[subsystem_label] = scoped_label

    stale_labels = _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
    if stale_labels:
        common.remove_labels_from_merge_request(gl_project, gl_mergerequest.iid, stale_labels)

    ret = [f'Acks::{x}::{y}' for x, y in subsys_scoped_labels.items()]
    ret.sort()  # Sort the subsystem scoped labels for the tests.

    return (ret, subsys_without_acks)


def _get_ack_nack_summary(gl_project, gl_mergerequest, acks, nacks, all_reviewers, min_reviewers):
    # pylint: disable=too-many-arguments,too-many-locals
    (subsys_scoped_labels, subsys_without_acks) = _get_subsys_scoped_labels(gl_project,
                                                                            gl_mergerequest,
                                                                            acks, nacks,
                                                                            all_reviewers)
    summary = []
    ack_summary = _show_ack_nacks(acks)
    if ack_summary:
        summary.append(f'ACKed by {ack_summary}.')

    if subsys_without_acks:
        subsys_str = []
        for subsystem_reviewers in subsys_without_acks:
            # The sets are unsorted, so sort them so that we get consistent results for the tests.
            sorted_reviewers = list(subsystem_reviewers)
            sorted_reviewers.sort()
            subsys_str.append('(%s)' % (', '.join(sorted_reviewers)))

        subsys_str.sort()
        summary.append('Requires at least one ACK from the set(s) %s.' % (', '.join(subsys_str)))

    nack_summary = _show_ack_nacks(nacks)
    if nack_summary:
        summary.append(f'NACKed by {nack_summary}.')
        return ('NACKed', subsys_scoped_labels, ' '.join(summary))

    if subsys_without_acks:
        return ('NeedsReview', subsys_scoped_labels, ' '.join(summary))

    rh_acks = [x for x in acks if x[1].endswith(('@redhat.com', '@fedoraproject.org'))]
    if len(rh_acks) < min_reviewers:
        summary.append(f'Requires {min_reviewers - len(rh_acks)} more ACK(s).')
        return ('NeedsReview', subsys_scoped_labels, ' '.join(summary))

    return ('OK', subsys_scoped_labels, ' '.join(summary))


def _lookup_gitlab_email(gl_instance, user_id, user_cache):
    if user_id in user_cache:
        return user_cache[user_id]

    user_cache[user_id] = gl_instance.users.get(user_id).public_email
    return user_cache[user_id]


def _lookup_submitter_and_notes(gl_instance, gl_mergerequest, user_cache):
    notes = []
    for note in gl_mergerequest.notes.list(sort='asc', order_by='created_at', as_list=False):
        email = _lookup_gitlab_email(gl_instance, note.author['id'], user_cache)
        notes.append((note.updated_at, note.body, email, note.author['username']))

    return notes


def _get_last_commit_timestamp(gl_mergerequest):
    """Walk all of the commits and get the oldest committed_date."""
    last_commit_timestamp = None
    for commit in gl_mergerequest.commits():
        if not last_commit_timestamp or last_commit_timestamp < commit.committed_date:
            last_commit_timestamp = commit.committed_date

    return last_commit_timestamp


def _filter_stale_reviewers(gl_mergerequest, stale):
    # this should filter out reviewers added excplicity by non-bot users
    user_added = []
    bot_added = []
    notes = gl_mergerequest.notes.list(as_list=False)
    for note in notes:
        n_author = note.author['username']
        n_body = note.body
        if n_body.startswith("requested review from"):
            if n_author in common.BOT_ACCOUNTS:
                for user in stale:
                    if f'@{user}' in n_body:
                        bot_added.append(user)
            else:
                for user in stale:
                    if f'@{user}' in n_body:
                        user_added.append(user)

    return [x for x in stale if x not in user_added and x in bot_added]


def _unassign_reviewers(gl_mergerequest, users):
    if users and misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        LOGGER.info('Unassigning users %s to MR %s', users, gl_mergerequest.iid)

        # Assign the reviewers via a quick action to avoid race conditions.
        gl_mergerequest.notes.create({'body': '/unassign_reviewer ' + ' '.join(users)})


def _get_existing_reviewers(gl_mergerequest):
    return [x['username'] for x in gl_mergerequest.reviewers]


def _emails_to_gl_user_names(gl_instance, reviewers):
    # Search GitLab for users with a public email address set.
    users = set([])
    for reviewer in reviewers:
        users.update([x.username for x in gl_instance.users.list(search=reviewer)])
    return users


def _get_stale_reviewers(gl_instance, old, all_reviewers):
    reviewers = set([])
    for reviewer_set in all_reviewers:
        reviewers.update(reviewer_set[0])
    new = _emails_to_gl_user_names(gl_instance, reviewers)
    return [x for x in old if x not in new]


def _do_assign_reviewers(gl_mergerequest, users):
    if users and misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        LOGGER.info('Assigning users %s to MR %s', users, gl_mergerequest.iid)

        # Assign the reviewers via a quick action to avoid race conditions.
        cmds = [f'/assign_reviewer @{x}' for x in users]
        gl_mergerequest.notes.create({'body': '\n'.join(cmds)})


def _assign_reviewers(gl_instance, gl_mergerequest, all_reviewers, submitter_email):
    # Gather the list of expected reviewers/maintainers from owners.yaml and remove MR submitter.
    reviewers = set([])
    for reviewer_set in all_reviewers:
        reviewers.update(reviewer_set[0])

    if submitter_email in reviewers:
        reviewers.remove(submitter_email)

    users = _emails_to_gl_user_names(gl_instance, reviewers)
    # A submitter w/o a public email can end up in reviewers still
    if gl_mergerequest.author['username'] in users:
        users.remove(gl_mergerequest.author['username'])

    LOGGER.info('Minimum reviewers on MR %s: %s', gl_mergerequest.iid, users)

    # Check the GitLab merge request reviewers field and remove users that are already assigned.
    for reviewer in gl_mergerequest.reviewers:
        if reviewer['username'] in users:
            users.remove(reviewer['username'])

    _do_assign_reviewers(gl_mergerequest, users)


def process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note):
    # pylint: disable=too-many-arguments,too-many-locals
    """Process a merge request."""
    dep_label = cdlib.set_dependencies_label(gl_instance, gl_project, gl_mergerequest)
    if dep_label not in gl_mergerequest.labels:
        gl_mergerequest = gl_project.mergerequests.get(gl_mergerequest.iid)
    if dep_label == "Dependencies::OK":
        changed_files = \
            [change['new_path'] for change in gl_mergerequest.changes()['changes']]
    else:
        changed_files = cdlib.get_filtered_changed_files(gl_mergerequest)

    # If get_min_reviewers() returns any reviewers just use that as our all_reviewers set.
    (min_reviewers, all_reviewers) = get_min_reviewers(gl_project.id, gl_mergerequest,
                                                       changed_files)

    user_cache = {}
    submitter_email = _lookup_gitlab_email(gl_instance, gl_mergerequest.author['id'],
                                           user_cache)

    old_reviewers = _get_existing_reviewers(gl_mergerequest)
    LOGGER.debug("Old reviewers: %s", old_reviewers)

    if not all_reviewers:
        all_reviewers = _get_required_reviewers(changed_files, owners_parser, submitter_email)
    LOGGER.debug("All reviewers: %s", all_reviewers)

    stale_reviewers = _get_stale_reviewers(gl_instance, old_reviewers, all_reviewers)
    if stale_reviewers:
        stale_reviewers = _filter_stale_reviewers(gl_mergerequest, stale_reviewers)
        LOGGER.debug("Stale reviewers: %s", stale_reviewers)
        _unassign_reviewers(gl_mergerequest, stale_reviewers)

    _assign_reviewers(gl_instance, gl_mergerequest, all_reviewers, submitter_email)

    notes = _lookup_submitter_and_notes(gl_instance, gl_mergerequest, user_cache)

    last_commit_timestamp = _get_last_commit_timestamp(gl_mergerequest)
    (acks, nacks) = _process_acks_nacks(notes, last_commit_timestamp, submitter_email,
                                        gl_instance, gl_project, gl_mergerequest, rhkernel_src)

    LOGGER.debug('Changed files: %s', changed_files)
    LOGGER.debug('Minimum reviewers for this MR: %d.', min_reviewers)
    LOGGER.debug('List of possible required reviewers: %s', all_reviewers)
    LOGGER.debug('List of ACKs: %s', acks)
    LOGGER.debug('List of NACKs: %s', nacks)

    summary = _get_ack_nack_summary(gl_project, gl_mergerequest, acks, nacks,
                                    all_reviewers, min_reviewers)
    _save(gl_instance, gl_project, gl_mergerequest, create_gl_status_note, *summary)


def _approve_button_clicked(gl_instance, gl_mergerequest, msg, tag, button_name):
    # GitLab delivers in the payload an email field and it's the Email field from
    # the user's profile. This is a different field than the Public email field on the
    # profile and the two can be different. Prefer the public email address if it's set
    # since that's used elsewhere and we've told people to make their @redhat.com address
    # their public email address.
    email = _lookup_gitlab_email(gl_instance, msg.payload['user']['id'], {})
    if not email:
        email = msg.payload['user']['email']

    name_email = '%s <%s>' % (msg.payload['user']['name'], email)
    body = f'{tag}: {name_email}\n(via {button_name} button)'
    LOGGER.debug('Leaving comment %s', body)

    if misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        gl_mergerequest.notes.create({'body': body})


def process_mr_webhook(gl_instance, msg, owners_parser, rhkernel_src):
    """Process a merge request only if a label was changed."""
    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    gl_mergerequest = gl_project.mergerequests.get(msg.payload['object_attributes']['iid'])

    gl_action = msg.payload['object_attributes'].get('action', '')
    if gl_action in ('approved', 'approval'):
        _approve_button_clicked(gl_instance, gl_mergerequest, msg, 'Acked-by', 'approve')
    elif gl_action in ('unapproved', 'unapproval'):
        _approve_button_clicked(gl_instance, gl_mergerequest, msg, 'Rescind-acked-by', 'unapprove')

    create_gl_status_note = common.has_label_changed(msg.payload, 'Acks::', common.LabelPart.PREFIX)
    process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note)

    return True


def process_note_webhook(gl_instance, msg, owners_parser, rhkernel_src):
    """Process a note message only if a tag was specified."""
    notetext = msg.payload['object_attributes']['note']
    create_gl_status_note = common.force_webhook_evaluation(notetext, 'ack-nack')

    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    gl_mergerequest = gl_project.mergerequests.get(msg.payload['merge_request']['iid'])

    (tag, _, note_email) = _parse_tag(notetext)
    if tag:
        user_cache = {}
        submitter_email = _lookup_gitlab_email(gl_instance, gl_mergerequest.author['id'],
                                               user_cache)
        public_email = _lookup_gitlab_email(gl_instance, msg.payload['user']['id'], user_cache)
        tag_error = _tag_email_error_message(tag, submitter_email, note_email, public_email,
                                             msg.payload['user']['username'])
        if tag_error:
            LOGGER.warning(tag_error)
            if misc.is_production() and not common.mr_is_closed(gl_mergerequest):
                gl_mergerequest.notes.create({'body': tag_error})
            return
    elif not create_gl_status_note:
        LOGGER.debug('Skipping note: %s', notetext)
        return

    process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note)


WEBHOOKS = {
    'merge_request': process_mr_webhook,
    'note': process_note_webhook,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args)
    owners_parser = common.get_owners_parser(args.owners_yaml)
    common.generic_loop(args, WEBHOOKS, owners_parser=owners_parser, rhkernel_src=args.rhkernel_src)


if __name__ == '__main__':
    main(sys.argv[1:])
