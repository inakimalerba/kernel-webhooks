"""Manages the cki:: label."""
import enum
import sys

from cki_lib import logger
from cki_lib import session

from . import common

LOGGER = logger.get_logger('cki.webhook.ckihook')
SESSION = session.get_session('cki.webhook.ckihook')

LABEL_PREFIX = 'CKI'

UNKNOWN_LABEL = "This MR's latest pipeline is in an unexpected state."
MISSING_LABEL = "This MR is missing a pipeline."
FAILED_LABEL = "This MR's latest pipeline has failed or been canceled."
RUNNING_LABEL = "This MR's latest pipeline is still running."
SUCCESS_LABEL = "This MR's latest pipeline was successful."


class Status(enum.IntEnum):
    # pylint: disable=invalid-name
    """Possible status of a pipeline."""

    Unknown = 0
    Missing = 1
    Failed = 2
    Canceled = 2
    Running = 3
    Pending = 3
    Success = 4


def get_failed_stage(gl_instance, gl_project, pipeline_id):
    """Return the Stage the pipeline failed on, if any."""
    pipeline = gl_project.pipelines.get(pipeline_id)

    # Get the bridge job for the main KERNEL_PIPELINE.
    bridge_job = next((bridge for bridge in pipeline.bridges.list()
                       if bridge.name == common.KERNEL_PIPELINE), None)
    if not bridge_job:
        LOGGER.info('Pipeline does not have expected bridge job %s.', common.KERNEL_PIPELINE)
        return None

    # Get to that downstream pipeline...
    if not bridge_job.downstream_pipeline:
        LOGGER.info('Bridge job pipeline does not have a downstream pipeline.')
        return None
    downstream_namespace = common.parse_gl_project_path(bridge_job.downstream_pipeline['web_url'])
    downstream_project = gl_instance.projects.get(downstream_namespace)
    downstream_pipeline = downstream_project.pipelines.get(bridge_job.downstream_pipeline.get('id'))
    if not downstream_pipeline:
        LOGGER.info('Could not get downstream pipeline.')
        return None

    return next((job.stage for job in downstream_pipeline.jobs.list(as_list=False) if
                 job.status == 'failed'), None)


def get_pipeline_status(gl_instance, gl_project, pipeline):
    """Return the status of the pipeline."""
    if pipeline is None:
        return (Status.Missing, None)

    # It is possible that a pipeline was triggered in an unexpected namespace.
    if not pipeline.get('web_url', '').startswith('https://gitlab.com/redhat/'):
        return (Status.Unknown, None)

    pipe_status = pipeline.get('status', 'unknown').capitalize()
    status = next((member for name, member in Status.__members__.items() if pipe_status == name),
                  Status.Unknown)

    failed_stage = get_failed_stage(gl_instance, gl_project, pipeline['id']) if \
        status is Status.Failed else None

    return (status, failed_stage)


def label_string(status, failed_stage):
    """Make the label string."""
    return f'{LABEL_PREFIX}::{status.name}::{failed_stage}' if failed_stage \
           else f'{LABEL_PREFIX}::{status.name}'


def add_pipeline_label_to_mr(gl_instance, gl_project, mr_id, status, failed_stage):
    """Update the label of the given MR."""
    label_color = common.NEEDS_REVIEW_LABEL_COLOR
    label_desc = globals()[f'{status.name.upper()}_LABEL']
    if status is Status.Success:
        label_color = common.READY_LABEL_COLOR
    if status is Status.Running:
        label_color = common.NEEDS_TESTING_LABEL_COLOR

    label_name = label_string(status, failed_stage)
    label = common.create_label_object(label_name, label_color, label_desc)
    common.add_label_to_merge_request(gl_instance, gl_project, mr_id, [label])


def compute_label(gl_instance, project_id, mr_id):
    """Compute and apply a CKI label for the given MR."""
    gl_project = gl_instance.projects.get(project_id)
    merge_request = gl_project.mergerequests.get(mr_id)

    current_label = next((label for label in merge_request.labels if
                          label.startswith(f'{LABEL_PREFIX}::')), None)
    LOGGER.info('MR %d CKI existing label is: %s', mr_id, current_label)

    (status, failed_stage) = get_pipeline_status(gl_instance, gl_project,
                                                 merge_request.head_pipeline)

    # If the pipeline status label hasn't changed then do nothing.
    new_label = label_string(status, failed_stage)
    if new_label == current_label:
        LOGGER.info('MR %d CKI label has not changed.', mr_id)
        return

    LOGGER.info('MR %d CKI status is now: %s', mr_id, status.name)

    # Gitlab does not automagically remove double scoped labels the way I expect 😠
    if current_label:
        common.remove_labels_from_merge_request(gl_project, mr_id, [current_label])
    add_pipeline_label_to_mr(gl_instance, gl_project, mr_id, status, failed_stage)


def process_pipeline(gl_instance, msg):
    """Process a pipeline event."""
    compute_label(gl_instance, msg.payload["project"]["id"], msg.payload["merge_request"]["iid"])


def process_mr(gl_instance, msg):
    """Process a merge_request event."""
    mr_id = msg.payload["object_attributes"]["iid"]
    # If the MR already has a CKI label then ignore this MR event as any existing
    # label will be kept up to date by pipeline events.
    if msg.payload.get('labels') and [label for label in msg.payload['labels'] if
                                      label['title'].startswith(f'{LABEL_PREFIX}::')]:
        LOGGER.info('MR %d already has a CKI label, ignoring MR event.', mr_id)
        return

    # The MR doesn't have a CKI label so compute one.
    compute_label(gl_instance, msg.payload["project"]["id"], mr_id)


WEBHOOKS = {
    'merge_request': process_mr,
    'pipeline': process_pipeline,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
