#!/usr/bin/env bash

# This script runs garbage collection on the upstream and rhel kernel
# git repo mirrors we maintain for the kernel webhooks.

set -e

ABSPATH=${BASH_SOURCE[0]}
BASEDIR=$(dirname ${ABSPATH})
LINUX_SRC=${LINUX_SRC:-/usr/src/linux}
RHKERNEL_SRC=${RHKERNEL_SRC:-/usr/src/rhkernel}
RETVAL=0

if [ -d ${LINUX_SRC}/.git ]; then
  echo "Garbage collecting upstream kernel git repo in ${LINUX_SRC}"
  git gc
else
  echo "No upstream kernel git repo found"
  let RETVAL+=1
fi

if [ -d ${RHKERNEL_SRC}/.git ]; then
  echo "Garbage collecting rhel kernel git repo in ${RHKERNEL_SRC}"
  git gc
else
  echo "No rhel kernel git repo found"
  let RETVAL+=1
fi

echo "All done."
exit ${RETVAL}
