#!/usr/bin/env bash

set -e

# The ACK/NACK webhook needs to write out some temporary files into the skeleton
# linux tree. OpenShift sets up the root filesystem inside the container as
# readonly. We can write into /data though. When the container starts, copy
# the cached copy that was 

export OWNERS_YAML=/data/owners.yaml
export RHKERNEL_SRC=/data/rhkernel

exec cki_entrypoint.sh
