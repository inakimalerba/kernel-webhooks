"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

import webhook.ack_nack
import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestAckNack(unittest.TestCase):
    PAYLOAD_NOTE = {'object_kind': 'note',
                    'user': {'id': 1, 'username': 'user1'},
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'iid': 1,
                                          'noteable_type': 'MergeRequest',
                                          'note': 'comment',
                                          'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    def test_get_required_reviewers(self):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        owners_parser = webhook.owners.Parser(owners_yaml)
        reviewers = webhook.ack_nack._get_required_reviewers(['redhat/Makefile'], owners_parser,
                                                             'user1@redhat.com')
        # user1@redhat.com is the merge request submitter and shouldn't be in the reviewer list.
        self.assertEqual(reviewers, [(frozenset(['user2@redhat.com']), None)])

        reviewers = webhook.ack_nack._get_required_reviewers([], owners_parser, 'user1@redhat.com')
        self.assertEqual(reviewers, set([]))

    def test_get_required_reviewers_no_self_review(self):
        # The person that opened the merge request is the only person listed as a subsystem
        # maintainer. That person should not be listed as a required reviewer.
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        owners_parser = webhook.owners.Parser(owners_yaml)
        reviewers = webhook.ack_nack._get_required_reviewers(['redhat/Makefile'], owners_parser,
                                                             'user1@redhat.com')
        self.assertEqual(reviewers, [])

    def test_get_ark_config_mr_ccs(self):
        mr = mock.Mock()
        mr.description = ''
        self.assertEqual(webhook.ack_nack.get_ark_config_mr_ccs(mr), [])
        mr.description = 'Cool description.\nCc: user1 <user1@redhat.com>\n'
        self.assertEqual(webhook.ack_nack.get_ark_config_mr_ccs(mr), ['user1@redhat.com'])

    @mock.patch('webhook.ack_nack.get_ark_config_mr_ccs')
    def test_get_min_reviewers(self, mocked_getarkconfig):
        mocked_mr = mock.Mock()
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = []
        # For non-ark project the function should return MIN_REVIEWERS.
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(12345, mocked_mr, ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)
        # Ark MR that touches non-config files function should return MIN_REVIEWERS.
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.common.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi', 'net/core/dev.c'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)
        # Ark MR that only touches config files and has CCs should return MIN_ARK_CONFIG_REVIEWERS.
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = ['ark_user1@redhat.com']
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.common.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_ARK_CONFIG_REVIEWERS)
        self.assertEqual(cc_reviewers, [({'ark_user1@redhat.com'}, None)])
        # If get_ark_config_mr_ccs() returns nothing then we expect MIN_REVIEWERS.
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = []
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.common.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)

    def test_parse_tag(self):
        tag = webhook.ack_nack._parse_tag('Acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('LGTM!\n\nAcked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Rescind-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Revoke-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Rescind-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Revoke-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Unknown-tag: User 3 <user3@redhat.com>')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag('You forgot your Acked-by: User 1 <user1@redhat.com>')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag('Acked-by: Invalid Format')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag(('Acked-by: User 1 <user1@redhat.com>\n'
                                           '\n'
                                           '> patch title\n'
                                           '> Signed-off-by: User 2 <user2@redhat.com>\n'))
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=True))
    @mock.patch('webhook.cdlib.get_last_code_changed_timestamp')
    def test_process_acks_nacks(self, ccts):
        ccts.return_value = None
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked',
                                                             'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com'),
                                    ('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        ccts.return_value = '2021-05-01T19:40:00.000Z'
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked',
                                                             'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com'),
                                    ('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        ccts.return_value = None
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Rescind-acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked',
                                                             'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([('User 2', 'user2@redhat.com')]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@edhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Another comment', 'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:52:00.000Z', 'Revoke-nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked',
                                                             'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user1@redhat.com',
                     'user1'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked',
                                                             'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Don't allow self ACKs
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user3@redhat.com', 'user3'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>',
                     'user3@redhat.com', 'user3'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked',
                                                             'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # Primary email not set on account
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user3@redhat.com', 'user3'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>',
                     'user3@redhat.com', 'user3'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             None, 'mocked', 'mocked', 'mocked',
                                                             'mocked')
        self.assertEqual(acks, set([('User 3', 'user3@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        # This Ack should not be counted since it's before the last_commit_timestamp
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Acked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-10T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked',
                                                             'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Do not fail if the payload object_attributes does not have a last_commit attribute.
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user1@redhat.com',
                     'user1'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Ignore ACK with forged address
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'someuser@gmail.com', 'someuser'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'someuser@gmail.com', 'someuser'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # Public from address not set
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'someuser@gmail.com', 'someuser'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Acked-by: User 1 <user1@redhat.com>', None,
                     'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # cki-bot can impersonate users
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z',
                     'Some User user1@redhat.com commented via email:\n'
                     'Acked-by: User 1 <user1@redhat.com>', None, 'cki-bot'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # make sure email bridge acks aren't forged either
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z',
                     'Some User <baduser@redhat.com> commented via email:\n'
                     'Acked-by: User 1 <user1@redhat.com>', None,
                     webhook.common.EMAIL_BRIDGE_ACCOUNT))
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                                 'mocked', 'mocked', 'mocked',
                                                                 'mocked')
            self.assertEqual(acks, set([]))
            self.assertEqual(nacks, set([]))

            self.assertIn(('Rejecting apparent forged ack/nack attempt (baduser@redhat.com '
                           '!= user1@redhat.com)'),
                          logs.output[-1])

    @mock.patch('webhook.ack_nack._get_old_subsystems_from_labels')
    def test_get_ack_nack_summary(self, old_subsystems):
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()
        gl_mergerequest.resourcelabelevents.list.return_value = []
        old_subsystems.return_value = ([], [])
        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [], [], [], 2),
                         ('NeedsReview', [], 'Requires 2 more ACK(s).'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com')],
                                                                [], [], 2),
                         ('NeedsReview', [],
                          'ACKed by User 1 <user1@redhat.com>. Requires 1 more ACK(s).'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], [], 2),
                         ('OK', [],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [],
                                                                [(['user1@redhat.com',
                                                                   'user2@redhat.com'],
                                                                  'subsys1')],
                                                                2),
                         ('OK', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [],
                                                                [(['user1@redhat.com',
                                                                   'user3@redhat.com'],
                                                                  'subsys1')],
                                                                2),
                         ('OK', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [('User 3', 'user3@redhat.com')],
                                                                [(['user1@redhat.com',
                                                                   'user2@redhat.com'],
                                                                  'subsys1')],
                                                                2),
                         ('NACKed', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>. ' +
                          'NACKed by User 3 <user3@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com'),
                                                                 ('User 3', 'user3@redhat.com')],
                                                                [],
                                                                [(['user1@redhat.com',
                                                                   'user2@redhat.com'],
                                                                  'subsys1'),
                                                                 (['user4@redhat.com',
                                                                   'user5@redhat.com'],
                                                                  'subsys2')],
                                                                2),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>, ' +
                          'User 3 <user3@redhat.com>. Requires at least one ACK from the set(s) ' +
                          '(user4@redhat.com, user5@redhat.com).'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 4', 'user4@redhat.com')],
                                                                [('User 2', 'user2@redhat.com')],
                                                                [(['user1@redhat.com',
                                                                   'user2@redhat.com'],
                                                                  'subsys1'),
                                                                 (['user4@redhat.com',
                                                                   'user5@redhat.com'],
                                                                  'subsys2')],
                                                                2),
                         ('NACKed', ['Acks::subsys1::NACKed', 'Acks::subsys2::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 4 <user4@redhat.com>. ' +
                          'NACKed by User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com'),
                                                                 ('User 3', 'user3@redhat.com')],
                                                                [],
                                                                [(['user1@redhat.com',
                                                                   'user2@redhat.com'],
                                                                  'subsys1'),
                                                                 (['user4@redhat.com',
                                                                   'user5@redhat.com'],
                                                                  'subsys2'),
                                                                 (['user6@redhat.com',
                                                                   'user7@redhat.com'],
                                                                  'subsys3'),
                                                                 (['user1@redhat.com',
                                                                   'user8@redhat.com'],
                                                                  'subsys4')],
                                                                2),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview',
                                          'Acks::subsys3::NeedsReview', 'Acks::subsys4::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>, ' +
                          'User 3 <user3@redhat.com>. Requires at least one ACK from the set(s) ' +
                          '(user4@redhat.com, user5@redhat.com), ' +
                          '(user6@redhat.com, user7@redhat.com).'))

        # External email address
        self.assertEqual(webhook.ack_nack._get_ack_nack_summary(gl_project, gl_mergerequest,
                                                                [('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@gmail.com')],
                                                                [],
                                                                [(['user1@redhat.com',
                                                                   'user2@redhat.com'],
                                                                  'subsys1')],
                                                                2),
                         ('NeedsReview', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@gmail.com>. ' +
                          'Requires 1 more ACK(s).'))

    @mock.patch('webhook.cdlib.set_dependencies_label')
    @mock.patch('webhook.cdlib.get_filtered_changed_files')
    @mock.patch('webhook.ack_nack._do_assign_reviewers')
    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=True))
    def test_process_merge_request1(self, assign_reviewers, get_files, set_label):
        get_files.return_value = ['include/linux/netdevice.h']
        set_label.return_value = "Dependencies::OK"

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = webhook.owners.Parser(owners_yaml)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: Impersonated User <notuser3@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 3))

        gl_instance = mock.Mock()
        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}]}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.commits.return_value = \
            [mock.Mock(committed_date='2021-01-08T20:00:00.000Z')]
        gl_mergerequest.reviewers = []
        gl_mergerequest.resourcelabelevents.list.return_value = []

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        gl_project.commits.get.return_value = mock.Mock(parent_ids=["1234"])

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(public_email=f'user{author_id}@redhat.com')
        gl_instance.users.list = \
            lambda search: [mock.Mock(public_email=search, username=search.split('@')[0])]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   owners_parser, 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.'))
            assign_reviewers.assert_called_with(gl_mergerequest, set(['user1', 'user2']))

        set_label.return_value = 'Dependencies::deadbeefabcd'
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   owners_parser, 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.'))
            get_files.assert_called_once()

        gl_project.id = webhook.common.ARK_PROJECT_ID
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   owners_parser, 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.'))

    @mock.patch('webhook.common.extract_all_bzs')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=True))
    def test_process_merge_request2(self, ext_deps, ext_bzs):
        ext_deps.return_value = []
        ext_bzs.return_value = ([], [], [])

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = webhook.owners.Parser(owners_yaml)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        # Code is pushed up based on committed_date below; previous ack is no longer valid
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-10T07:00:00.000Z', 2))

        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}]}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.commits.return_value = \
            [mock.Mock(committed_date='2021-01-10T03:00:00.000Z')]
        gl_mergerequest.reviewers = []
        gl_mergerequest.resourcelabelevents.list.return_value = []

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(public_email=f'user{author_id}@redhat.com')
        gl_instance.users.list.return_value = [mock.Mock(public_email='user1@redhat.com',
                                                         username='user1')]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   owners_parser, 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: NeedsReview '
                              '- ACKed by User 2 <user2@redhat.com>. Requires 1 more ACK(s).'))

    def __create_note_body(self, body, timestamp, author_id):
        ret = mock.Mock()
        ret.body = body
        ret.updated_at = timestamp
        ret.author = {'id': author_id, 'username': f'user{author_id}'}
        return ret

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message0(self, mocked_process_mr, mock_gl):
        self._test_note("request-ack-nack-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message1(self, mocked_process_mr, mock_gl):
        self._test_note("request-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message2(self, mocked_process_mr, mock_gl):
        self._test_note("Acked-by: User 1 <user1@redhat.com>", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message3(self, mocked_process_mr, mock_gl):
        # Merge request author public email will be set to user2@redhat.com below
        # so this should fail since it's a user trying to self-ack a patch.
        with self.assertLogs('cki.webhook.ack_nack', level='WARNING') as logs:
            self._test_note("Acked-by: User 2 <user2@redhat.com>", mocked_process_mr, mock_gl)
            mocked_process_mr.assert_not_called()
            self.assertIn("user2@redhat.com cannot self-ack merge request", logs.output[-1])

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message4(self, mocked_process_mr, mock_gl):
        # User's public email doesn't match the address in the Acked-by so this should fail.
        with self.assertLogs('cki.webhook.ack_nack', level='WARNING') as logs:
            self._test_note("Acked-by: User 1 <user1@gmail.com>", mocked_process_mr, mock_gl)
            mocked_process_mr.assert_not_called()
            self.assertIn(("Ignoring 'Acked-by <user1@gmail.com>' since this doesn't match "
                           "the user's public email address user1@redhat.com on GitLab"),
                          logs.output[-1])

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message5(self, mocked_process_mr, mock_gl):
        self._test_note("Some comment", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_not_called()

    def _test_note(self, note_text, mocked_process_mr, mock_gl):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = mock.Mock(author={'id': 2, 'username': 'user1'})
        gl_instance.projects.get.return_value = gl_project
        gl_instance.users.get = lambda user_id: mock.Mock(public_email=f'user{user_id}@redhat.com',
                                                          username=f'user{user_id}')
        mock_gl.return_value.__enter__.return_value = gl_instance

        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = note_text
        self.assertTrue(webhook.common.process_message("ROUTING_KEY", payload,
                                                       webhook.ack_nack.WEBHOOKS,
                                                       False, owners_parser='mocked',
                                                       rhkernel_src='mocked'))

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_other_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Change a label not related to ack/nack
        msg.payload['changes']['labels']['previous'] = [{'title': 'Acks::OK'},
                                                        {'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'}]

        owners_parser = mock.Mock()
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, owners_parser,
                                                            'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked', False)

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_ack_nack_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Add ack_nack label
        msg.payload['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'},
                                                       {'title': 'somethingelse::OK'}]

        owners_parser = mock.Mock()
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, owners_parser,
                                                            'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked', True)

    def _create_merge_payload(self, action):
        return {'object_kind': 'merge_request',
                'user': {'id': 1, 'name': 'User 1', 'email': 'user1@redhat.com'},
                'project': {'id': 1, 'web_url': 'https://web.url/g/p'},
                'object_attributes': {'iid': 2, 'action': action,
                                      'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                'changes': {'labels': {'previous': [], 'current': []}}}

    def _test_approve_button_with(self, public_email, expected_email):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_mergerequest.notes = mock.Mock()
        gl_mergerequest.notes.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_instance.users.get.return_value = mock.Mock(public_email=public_email)

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('approved')
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': f'Acked-by: User 1 <{expected_email}>\n(via approve button)'})

        msg.payload = self._create_merge_payload('unapproved')
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': f'Rescind-acked-by: User 1 <{expected_email}>\n(via unapprove button)'})

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.ack_nack.process_merge_request', mock.Mock())
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_approve_button(self):
        # The test payload has user1@redhat.com hardcoded as the email field. Try various
        # public email addresses.
        self._test_approve_button_with('public-email@redhat.com', 'public-email@redhat.com')
        self._test_approve_button_with('user1@redhat.com', 'user1@redhat.com')
        self._test_approve_button_with(None, 'user1@redhat.com')
        self._test_approve_button_with('', 'user1@redhat.com')

    def test_filter_stale_reviewers(self):
        mock_mr = mock.Mock()
        stale_reviewers = ['someone', 'nobody']
        note1 = mock.Mock(id="1234", author={'username': 'cki-kwf-bot'},
                          body='requested review from @someone')
        note2 = mock.Mock(id="1235", author={'username': 'user35'},
                          body='requested review from @nobody')
        notes = mock.MagicMock()
        notes.list.return_value = [note1, note2]
        mock_mr.notes = notes
        output = webhook.ack_nack._filter_stale_reviewers(mock_mr, stale_reviewers)
        self.assertEqual(output, ['someone'])
        stale_reviewers = ['nobody', 'anotherone']
        output = webhook.ack_nack._filter_stale_reviewers(mock_mr, stale_reviewers)
        self.assertEqual(output, [])

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.common.mr_is_closed', mock.Mock(return_value=False))
    def test_assign_reviewers(self, mocked_users):
        mock_instance = mock.Mock()
        mock_mr = mock.MagicMock()
        mock_mr.iid = 222
        mock_mr.author = {'username': 'joeschmoe'}
        reviewers = ['joeschmoe@redhat.com', 'someone@redhat.com', 'nobody@redhat.com']
        sub_email = 'joeschmoe@redhat.com'
        assign = {'body': '/assign_reviewer @someone\n/assign_reviewer @nobody'}
        mocked_users.return_value = ['joeschmoe', 'someone', 'nobody']
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack._assign_reviewers(mock_instance, mock_mr, reviewers, sub_email)
            self.assertIn("Assigning users ['someone', 'nobody'] to MR 222", logs.output[-1])
            mock_mr.notes.create.assert_called_with(assign)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.common.mr_is_closed', mock.Mock(return_value=False))
    def test_unassign_reviewers(self):
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        users = ['someone', 'nobody']
        unassign = {'body': '/unassign_reviewer someone nobody'}
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack._unassign_reviewers(mock_mr, users)
            self.assertIn("Unassigning users ['someone', 'nobody'] to MR 2", logs.output[-1])
            mock_mr.notes.create.assert_called_with(unassign)

    def test_get_existing_reviewers(self):
        mock_mr = mock.Mock()
        rev1 = {'id': 1, 'name': 'Some One', 'username': 'someone'}
        rev2 = {'id': 2, 'name': 'No Body', 'username': 'nobody'}
        rev3 = {'id': 3, 'name': 'Red Hatter', 'username': 'redhatter'}
        mock_mr.reviewers = [rev1, rev2, rev3]
        output = webhook.ack_nack._get_existing_reviewers(mock_mr)
        self.assertEqual(output, ['someone', 'nobody', 'redhatter'])

    def test_emails_to_gl_user_names(self):
        mock_instance = mock.Mock()
        user1 = mock.Mock(id="1", name="Some One", username="someone")
        user2 = mock.Mock(id="2", name="No Body", username="nobody")
        user3 = mock.Mock(id="3", name="Red Hatter", username="redhatter")
        reviewers = ['someone@redhat.com', 'nobody@redhat.com', 'redhatter@redhat.com']
        mock_instance.users.list.return_value = [user1, user2, user3]
        output = webhook.ack_nack._emails_to_gl_user_names(mock_instance, reviewers)
        self.assertEqual(output, {'someone', 'nobody', 'redhatter'})

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    def test_get_stale_reviewers(self, user_names):
        mock_instance = mock.Mock()
        old = ['user3']
        all_reviewers = [(['user1@redhat.com', 'user2@redhat.com'], 'subsys1')]
        user_names.return_value = ['user1', 'user2']
        output = webhook.ack_nack._get_stale_reviewers(mock_instance, old, all_reviewers)
        self.assertEqual(output, ['user3'])

    def test_get_old_subsystems_from_labels(self):
        mock_mr = mock.Mock()
        mock_mr.labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Something::whatever']
        (subsys, labels) = webhook.ack_nack._get_old_subsystems_from_labels(mock_mr)
        self.assertEqual(subsys, ['net', 'mm'])
        self.assertEqual(labels, ['Acks::net::NeedsReview', 'Acks::mm::OK'])

    def test_get_stale_labels(self):
        old_subsystems = ['net', 'mm', 'foobar']
        old_labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Acks::foobar::NACKed']
        subsys_scoped_labels = {'net': 'NeedsReview', 'mm': 'OK'}
        output = webhook.ack_nack._get_stale_labels(old_subsystems, old_labels,
                                                    subsys_scoped_labels)
        self.assertEqual(output, ['Acks::foobar::NACKed'])
