"""Webhook interaction tests."""
import json
import os
import pathlib
import tempfile
import unittest
from unittest import mock

from gitlab.exceptions import GitlabCreateError

import webhook.common


class MyLister():
    def __init__(self, labels):
        self.labels = labels
        self.call_count = 0
        self.called_with_all = None

    def list(self, search=None, all=False):
        self.call_count += 1
        if all:
            self.called_with_all = True
            return self.labels
        self.called_with_all = False
        if not search:
            raise(AttributeError)
        return [label for label in self.labels if label.name == search]


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommon(unittest.TestCase):
    PROJECT_LABELS = [{'id': 1,
                       'name': 'readyForMerge',
                       'color': webhook.common.READY_FOR_MERGE_LABEL['color'],
                       'description': ('All automated checks pass, this merge request'
                                       ' should be suitable for inclusion in main now.'),
                       'text_color': '#FFFFFF',
                       'priority': 1
                       },
                      {'id': 2,
                       'name': 'readyForQA',
                       'color': webhook.common.READY_FOR_QA_LABEL['color'],
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 3,
                       'name': 'Acks::NACKed',
                       'color': webhook.common.NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 4,
                       'name': 'Acks::NeedsReview',
                       'color': webhook.common.NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request needs more reviews and acks'
                                       ' from Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 3
                       },
                      {'id': 5,
                       'name': 'Acks::OK',
                       'color': webhook.common.READY_LABEL_COLOR,
                       'description': ('This merge request has been reviewed by Red Hat'
                                       ' engineering and approved for inclusion.'),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 6,
                       'name': 'Bugzilla::OK',
                       'color': webhook.common.READY_LABEL_COLOR,
                       'description': ("This merge request's bugzillas are approved for"
                                       " integration"),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 7,
                       'name': 'Bugzilla::NeedsTesting',
                       'color': webhook.common.NEEDS_TESTING_LABEL_COLOR,
                       'description': "This merge request's bugzillas are ready for QA testing.",
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 8,
                       'name': 'lnst::NeedsTesting',
                       'color': webhook.common.NEEDS_TESTING_LABEL_COLOR,
                       'description': ('The subsystem-required lnst testing has not yet'
                                       ' been completed.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 9,
                       'name': 'lnst::OK',
                       'color': webhook.common.READY_LABEL_COLOR,
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 10,
                       'name': 'Dependencies::abcdef012345',
                       'color': '#123456',
                       'description': ('This MR has dependencies.'),
                       'text_color': '#FFFFFF',
                       'priority': 15
                       }]

    def test_mr_is_closed(self):
        mrequest = mock.Mock()
        mrequest.state = "closed"
        status = webhook.common.mr_is_closed(mrequest)
        self.assertTrue(status)
        mrequest.state = "opened"
        status = webhook.common.mr_is_closed(mrequest)
        self.assertFalse(status)

    def test_build_note_string(self):
        notes = []
        notes += ["1"]
        notes += ["2"]
        notes += ["3"]
        notestring = webhook.common.build_note_string(notes)
        self.assertEqual(notestring, "See 1, 2, 3|\n")

    def test_build_commits_for_row(self):
        commits = []
        commits.append("abcdef012345")
        table = []
        table.append(["012345abcdef", commits, 1, "", ""])
        for row in table:
            commits = webhook.common.build_commits_for_row(row)
            self.assertEqual(commits, ["abcdef01"])

    def test_mr_action_affects_commits(self):
        """Check handling of a merge request with unwanted actions."""
        message = mock.Mock()
        # 'open' action should return True.
        payload = {'object_attributes': {'action': 'open'}}
        message.payload = payload
        self.assertTrue(webhook.common.mr_action_affects_commits(message))

        # 'update' action with oldrev set should return True.
        payload = {'object_attributes': {'action': 'update',
                                         'oldrev': 'hi'}
                   }
        message.payload = payload
        self.assertTrue(webhook.common.mr_action_affects_commits(message))

        # 'update' action without oldrev should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'update'}}
            message.payload = payload
            self.assertFalse(webhook.common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR \'update\' action without an oldrev.", logs.output[-1])

        # Action other than 'new' or 'update' should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'partyhard'}}
            message.payload = payload
            self.assertFalse(webhook.common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR action 'partyhard'", logs.output[-1])

        # Target branch changed should return True
        payload = {'object_attributes': {'action': 'update'},
                   'changes': {'merge_status': {'previous': 'can_be_merged',
                                                'current': 'unchecked'}}}
        message.payload = payload
        self.assertTrue(webhook.common.mr_action_affects_commits(message))

    def test_required_label_removed(self):
        payload = {'object_attributes': {'action': 'update'}}
        testing = webhook.common.NEEDS_TESTING_SUFFIX
        ready = webhook.common.READY_SUFFIX
        failed = webhook.common.TESTING_FAILED_SUFFIX
        waived = webhook.common.TESTING_WAIVED_SUFFIX

        changed_labels = ['lnst::NeedsTesting', 'whatever::OK']
        result = webhook.common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Drivers:bonding']
        result = webhook.common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['rdmalab::OK']
        result = webhook.common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Bugzilla::OK']
        result = webhook.common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['lnst::TestingFailed']
        result = webhook.common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['lnst::TestingFailed']
        result = webhook.common.required_label_removed(payload, failed, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['lnst::Waived']
        result = webhook.common.required_label_removed(payload, waived, changed_labels)
        self.assertEqual(result, True)

    def test_has_label_changed(self):
        msg = {}
        msg['changes'] = {}
        full = webhook.common.LabelPart.FULL
        prefix = webhook.common.LabelPart.PREFIX

        # No 'labels' key
        self.assertFalse(webhook.common.has_label_changed(msg, 'ack_nack', prefix))

        msg['changes']['labels'] = {}

        # No label changes
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertFalse(webhook.common.has_label_changed(msg, 'ack_nack::', prefix))

        # Label without prefix changes.
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'readyForMerge'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertTrue(webhook.common.has_label_changed(msg, 'readyForMerge', full))

        # Label without prefix does not change.
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'readyForMerge'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'},
                                               {'title': 'readyForMerge'}]
        self.assertFalse(webhook.common.has_label_changed(msg, 'readyForMerge', full))

        # Change a label not related to ack/nack
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertFalse(webhook.common.has_label_changed(msg, 'ack_nack::', prefix))

        # Remove ack_nack label
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'somethingelse::OK'}]
        self.assertTrue(webhook.common.has_label_changed(msg, 'ack_nack', prefix))

        # Add ack_nack label
        msg['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'},
                                               {'title': 'somethingelse::OK'}]
        self.assertTrue(webhook.common.has_label_changed(msg, 'ack_nack', prefix))

    def test_find_extra_required_labels(self):
        labels = ['lnst::NeedsTesting']
        result = webhook.common._find_extra_required_labels(labels)
        self.assertEqual(result, ['lnst::OK'])

    def _process_message(self, mock_gl, msg, auth_user):
        webhooks = {'note': mock.Mock()}

        fake_gitlab = mock.Mock(spec=[])

        def mock_auth():
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="mock_auth")

        fake_gitlab.auth = mock_auth
        if auth_user:
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="init")

        mock_gl.return_value.__enter__.return_value = fake_gitlab

        with tempfile.NamedTemporaryFile() as tmp:
            pathlib.Path(tmp.name).write_text(json.dumps(msg, indent=None))
            parser = webhook.common.get_arg_parser('test')
            args = parser.parse_args(['--json-message-file', tmp.name])
            webhook.common.consume_queue_messages(args, webhooks, True)

        if auth_user:
            self.assertEqual(fake_gitlab.user._setup_from, "init")
        else:
            self.assertEqual(fake_gitlab.user._setup_from, "mock_auth")

        return webhooks['note']

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_json_processing(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_no_auth(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, None)
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_bot_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'cki-bot'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_not_called()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_regular_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    def test_make_payload(self):
        # Note payload
        payload = webhook.common.make_payload('https://web.url/g/p/-/merge_requests/123', 'note')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload['object_attributes']['noteable_type'], 'MergeRequest')
        self.assertEqual(payload['merge_request']['iid'], 123)

        # MR payload
        payload = webhook.common.make_payload('https://web.url/g/p/-/merge_requests/123',
                                              'merge_request')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'merge_request')
        self.assertEqual(payload['object_attributes']['iid'], 123)
        self.assertFalse(payload['object_attributes']['work_in_progress'])

        # Pipeline payload
        payload = webhook.common.make_payload('https://web.url/g/p/-/merge_requests/123',
                                              'pipeline')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'pipeline')
        self.assertEqual(payload['merge_request']['iid'], 123)

        # Some other payload?
        payload = webhook.common.make_payload('https://web.url/g/p/-/merge_requests/123',
                                              'lion_attack')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'lion_attack')
        self.assertEqual(payload['_mr_id'], 123)

    def test_process_mr_url(self):
        url = 'https://web.url/g/p/-/merge_requests/123'

        # Note
        note_text = 'request-evaluation'
        payload = webhook.common.process_mr_url(url, 'note', note_text)
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload["object_attributes"]["note"], note_text)

        # MR
        payload = webhook.common.process_mr_url(url, 'merge_request', None)
        self.assertEqual(payload['object_kind'], 'merge_request')

        # Pipeline
        payload = webhook.common.process_mr_url(url, 'pipeline', None)
        self.assertEqual(payload['object_kind'], 'pipeline')

    @mock.patch('webhook.common.consume_queue_messages')
    @mock.patch('webhook.common.process_mr_url')
    @mock.patch('webhook.common.process_message')
    def test_generic_loop(self, mock_process_message, mock_mr_url, mock_consume):
        WEBHOOKS = {}
        mr_url = 'https://web.url/g/p/-/merge_requests/123'

        # --merge-request, no --note, no --action
        args = mock.Mock(merge_request=mr_url, action=None, note=None)
        webhook.common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'merge_request', None)
        mock_process_message.assert_called_with('cmdline', mock_mr_url.return_value, WEBHOOKS, True)

        # --merge-request with --note, no --action
        note_text = 'request-evaluation'
        args = mock.Mock(merge_request=mr_url, action=None, note=note_text)
        webhook.common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'note', note_text)
        mock_process_message.assert_called_with('cmdline', mock_mr_url.return_value, WEBHOOKS, True)

        # --merge-request and --action set, no --note.
        args = mock.Mock(merge_request=mr_url, action='pipeline', note=None)
        webhook.common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'pipeline', None)
        mock_process_message.assert_called_with('cmdline', mock_mr_url.return_value, WEBHOOKS, True)

        # no --merge-request
        mock_mr_url.reset_mock()
        mock_process_message.reset_mock()
        args = mock.Mock(merge_request=None)
        webhook.common.generic_loop(args, WEBHOOKS)
        mock_mr_url.assert_not_called()
        mock_process_message.assert_not_called()
        mock_consume.assert_called_with(args, WEBHOOKS, True)

    @mock.patch('json.dumps')
    def test_process_message(self, dumps):
        """Check for expected return value."""
        routing_key = 'FAKE_ROUTE_KEY'
        webhooks = {'merge_request': mock.Mock(), 'note': mock.Mock()}
        payload = {}

        # Wrong object kind should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'delete_project'
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, False))
            self.assertIn('Ignoring message from queue', logs.output[-1])

        # Closed state should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'closed'
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, False))
            self.assertIn("Ignoring event with 'closed' state.", logs.output[-1])

        # If username matches return False (event was generated by our own activity).
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bot')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'
            payload['user'] = {'username': 'bot'}
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, True))
            payload['user'] = {'username': 'notbot'}
            self.assertTrue(webhook.common.process_message(routing_key, payload, webhooks, True))

        # Ignore notes on issues.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'Issue'}
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, False))
            self.assertIn('Only processing notes related to merge requests', logs.output[-1])

        # Notes on merge requests should be processed.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs, \
             mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'MergeRequest'}
            self.assertTrue(webhook.common.process_message(routing_key, payload, webhooks, False))

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_plabel_commands(self, mr_labels, new_labels, expected_cmd, is_prod):
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = webhook.common.add_plabel_to_merge_request(gl_project, 2, new_labels)

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and label_ret:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and label_ret:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertIn("Editing label %s on project." % label['name'],
                                  ' '.join(logs.output))
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("Creating label {'name': '%s', 'color': '%s',"
                                    " 'description': '%s'} on project.") \
                                    % (label['name'], label['color'], label['description'])
                    self.assertIn(label_string, ' '.join(logs.output))
                    call_list.append(mock.call(label))
            if call_list:
                gl_project.labels.create.assert_has_calls(call_list)
            else:
                gl_project.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            self.assertTrue(label_ret)
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            self.assertFalse(label_ret)
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_label_commands(self, mr_labels, new_labels, expected_cmd, is_prod):
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_instance = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        gl_group = mock.Mock()
        gl_instance.groups.get.return_value = gl_group

        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = webhook.common.add_label_to_merge_request(gl_instance, gl_project,
                                                                  2, new_labels)

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and label_ret:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and label_ret:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertIn("Editing label %s on group." % label['name'],
                                  ' '.join(logs.output))
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("Creating label {'name': '%s', 'color': '%s',"
                                    " 'description': '%s'} on group.") \
                                    % (label['name'], label['color'], label['description'])
                    self.assertIn(label_string, ' '.join(logs.output))
                    call_list.append(mock.call(label))
            if call_list:
                gl_group.labels.create.assert_has_calls(call_list)
            else:
                gl_group.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            self.assertTrue(label_ret)
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            self.assertFalse(label_ret)
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._filter_mr_labels')
    @mock.patch('webhook.common._add_label_quick_actions', return_value=['/label "hello"'])
    @mock.patch('webhook.common._compute_mr_status_labels')
    def test_add_label_exception(self, mock_filter, mock_quickaction, mock_compute):
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        instance = mock.Mock()
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        label_list = [{'name': 'Acks::OK'}]

        # Catch "can't be blank" and move on.
        err_text = "400 Bad request - Note {:note=>[\"can't be blank\"]}"
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message=err_text,
                                                                  response_code=400,
                                                                  response_body='')
        exception_raised = False
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            with mock.patch('cki_lib.misc.is_production', return_value=True):
                try:
                    result = webhook.common.add_label_to_merge_request(instance, project,
                                                                       mergerequest.iid, label_list)
                except GitlabCreateError:
                    exception_raised = True
                self.assertTrue(result)
                self.assertFalse(exception_raised)
                self.assertIn("can't be blank", logs.output[-1])

        # For any other error let it bubble up.
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message='oops',
                                                                  response_code=403,
                                                                  response_body='')
        exception_raised = False
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            try:
                result = webhook.common.add_label_to_merge_request(instance, project,
                                                                   mergerequest.iid, label_list)
            except GitlabCreateError:
                exception_raised = True
        self.assertTrue(exception_raised)

    def test_add_plabel_to_merge_request(self):
        # Add a project label which does not exist on the project.
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::abcdef012345"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                      'description': 'This MR has dependencies.'}]
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands(mr_labels, new_labels, None)

    def test_add_label_to_merge_request(self):
        # Add a label which already exists on the project.
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Existing project label with new color.
        new_labels = [{'name': 'Acks::OK', 'color': '#123456'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Acks::OK'},
                     {'name': 'CommitRefs::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels, None)

        # Add a label which does not exist.
        new_labels = [{'name': 'NetSubsystem', 'color': '#123456',
                       'description': 'Net Subsystem label.'}]
        self._test_label_commands(mr_labels, new_labels, '/label "NetSubsystem"')

        # Add a label which triggers readyForMerge being added to the MR.
        mr_labels = [{'name': 'Bugzilla::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which triggers readyForMerge being removed from the MR.
        mr_labels += [{'name': 'Acks::OK'}, {'name': 'readyForMerge'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::NeedsReview"\n/unlabel "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add labels which trigger readyForTesting.
        mr_labels = [{'name': 'Acks::NeedsReview'}, {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'}, {'name': 'Signoff::OK'}]
        new_labels = [{'name': 'Acks::OK'}, {'name': 'Bugzilla::NeedsTesting'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "Bugzilla::NeedsTesting"\n'
                                   '/label "readyForQA"\n/unlabel "readyForMerge"'))

        # Add a double scoped label.
        new_labels = [{'name': 'BZ::123::OK', 'color': '#23456',
                       'description': 'Two scopes?'}]
        self._test_label_commands(mr_labels, new_labels, '/label "BZ::123::OK"')

        # Add two labels which both need to be created on the project. Use custom colors.
        new_labels = [{'name': 'Label 1', 'color': '#123456', 'description': 'description 1'},
                      {'name': 'Label 2', 'color': '#deadbe', 'description': 'description 2'}]
        self._test_label_commands(mr_labels, new_labels,
                                  '/label "Label 1"\n/label "Label 2"')

        # Add a NeedsTesting label.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'}]
        new_labels = [{'name': 'lnst::NeedsTesting'}]
        self._test_label_commands(mr_labels, new_labels, '/label "lnst::NeedsTesting"')

        # Add an lnst::OK label.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'},
                     {'name': 'lnst::NeedsTesting'}]
        new_labels = [{'name': 'lnst::OK'}]
        self._test_label_commands(mr_labels, new_labels, '/label "lnst::OK"')

        # Add a lot of labels to trigger labels.list(all=True).
        new_labels = []
        count = 1
        while count <= 8:
            name = f"Label {count}"
            label = {'name': name, 'color': '#123456', 'description': name}
            new_labels.append(label)
            count += 1
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Label 1"\n/label "Label 2"\n/label "Label 3"'
                                   '\n/label "Label 4"\n/label "Label 5"\n/label "Label 6"'
                                   '\n/label "Label 7"\n/label "Label 8"'))

    def test_extract_dependencies(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs = webhook.common.extract_dependencies(None)
        self.assertEqual(bzs, [])

        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n')
        bzs = webhook.common.extract_dependencies(message)
        self.assertEqual(bzs, ['22334455', '33445566'])

    def test_try_bugzilla_conn_no_key(self):
        """Check for negative return when BUGZILLA_API_KEY is not set."""
        self.assertFalse(webhook.common.try_bugzilla_conn())

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_try_bugzilla_conn_with_key(self):
        """Check for positive return when BUGZILLA_API_KEY is set."""
        with mock.patch('webhook.common.connect_bugzilla', return_value=True):
            self.assertTrue(webhook.common.try_bugzilla_conn())

    def test_connect_bugzilla(self):
        """Check for expected return value."""
        api_key = 'totally_fake_api_key'
        bzcon = mock.Mock()
        with mock.patch('bugzilla.Bugzilla', return_value=bzcon):
            self.assertEqual(webhook.common.connect_bugzilla(api_key), bzcon)

    @mock.patch('bugzilla.Bugzilla')
    def test_connect_bugzilla_exception(self, mocked_bugzilla):
        """Check ConnectionError exception generates logging."""
        api_key = 'totally_fake_api_key'
        mocked_bugzilla.side_effect = ConnectionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(webhook.common.connect_bugzilla(api_key))
            self.assertIn('Problem connecting to bugzilla server.', logs.output[-1])
        mocked_bugzilla.side_effect = PermissionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(webhook.common.connect_bugzilla(api_key))
            self.assertIn('Problem with file permissions', logs.output[-1])

    @mock.patch('webhook.common._edit_group_label')
    @mock.patch('webhook.common._edit_project_label')
    @mock.patch('webhook.common._match_label')
    def test_add_label_quick_actions(self, existing_label, plabel, glabel):
        """Check for the right label type (project or group) being evaluated."""
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        instance = mock.Mock()
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        label_list = [{'name': 'Acks::mm::OK'}]
        existing_label.return_value = ""
        output = webhook.common._add_label_quick_actions(instance, project, label_list)
        plabel.assert_not_called()
        glabel.assert_called_once()
        self.assertEqual(['/label "Acks::mm::OK"'], output)
        project.id = webhook.common.ARK_PROJECT_ID
        webhook.common._add_label_quick_actions(instance, project, label_list)
        plabel.assert_called_once()
        self.assertEqual(['/label "Acks::mm::OK"'], output)

    def test_extract_all_bzs(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs, non_mr_bzs, dep_bzs = webhook.common.extract_all_bzs(None, [], [])
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])

        message1 = 'Here is my perfect patch.\nBugzilla: https://bugzilla.redhat.com/18123456'
        bzs, non_mr_bzs, dep_bzs = webhook.common.extract_all_bzs(message1, [], [])
        self.assertEqual(bzs, ['18123456'])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        bzs, non_mr_bzs, dep_bzs = webhook.common.extract_all_bzs(message1, [], ['18123456'])
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, ['18123456'])
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            bzs, non_mr_bzs, dep_bzs = webhook.common.extract_all_bzs(message1, ['12345678'], [])
            self.assertIn(('Bugzilla: 18123456 not listed in MR description.'), logs.output[-1])
            self.assertEqual(bzs, [])
            self.assertEqual(non_mr_bzs, ['18123456'])
            self.assertEqual(dep_bzs, [])

    def test_parse_gl_project_path(self):
        url1 = 'http://www.gitlab.com/mycoolproject/-/merge_requests/12345'
        url2 = 'http://www.gitlab.com/group/subgroup/project/-/pipelines/35435747'
        url3 = 'https://gitlab.com/this/is/a/test/-/issues/'

        self.assertEqual('mycoolproject', webhook.common.parse_gl_project_path(url1))
        self.assertEqual('group/subgroup/project', webhook.common.parse_gl_project_path(url2))
        self.assertEqual('this/is/a/test', webhook.common.parse_gl_project_path(url3))
