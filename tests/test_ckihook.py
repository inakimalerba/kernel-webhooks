"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

from webhook import ckihook
from webhook import common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCkihook(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MR = {'object_kind': 'merge_request',
                  'project': {'id': 1},
                  'object_attributes': {'iid': 123}
                  }

    PAYLOAD_PIPELINE = {'object_kind': 'pipeline',
                        'project': {'id': 1},
                        'merge_request': {'iid': 123}
                        }

    def test_get_failed_stage(self):
        mock_gl = mock.Mock()
        mock_project = mock.Mock()

        # No bridge job.
        mock_pipeline = mock.Mock()
        mock_project.pipelines.get.return_value = mock_pipeline
        mock_pipeline.bridges.list.return_value = []

        result = ckihook.get_failed_stage(mock_gl, mock_project, 123)
        self.assertEqual(result, None)

        # No matching bridge job.
        bridge1 = mock.Mock()
        bridge1.name = common.KERNEL_RT_PIPELINE
        bridge2 = mock.Mock()
        bridge2.name = 'weird pipeline'
        mock_pipeline.bridges.list.return_value = [bridge1, bridge2]

        result = ckihook.get_failed_stage(mock_gl, mock_project, 123)
        self.assertEqual(result, None)

        # No downstream pipeline.
        bridge3 = mock.Mock()
        bridge3.name = common.KERNEL_PIPELINE
        bridge3.downstream_pipeline = {'web_url':
                                       'https://gitlab.com/group/project/-/pipelines/54321'}
        mock_pipeline.bridges.list.return_value = [bridge1, bridge2, bridge3]

        mock_ds_project = mock.Mock()
        mock_gl.projects.get.return_value = mock_ds_project
        mock_ds_project.pipelines.get.return_value = None

        result = ckihook.get_failed_stage(mock_gl, mock_project, 123)
        self.assertEqual(result, None)

        # No failed jobs.
        job1 = mock.Mock(status='success', stage='merge')
        job2 = mock.Mock(status='running', stage='build')
        job3 = mock.Mock(status='canceled', stage='prepare')
        mock_ds_pipeline = mock.Mock()
        mock_ds_pipeline.jobs.list.return_value = [job1, job2, job3]
        mock_ds_project.pipelines.get.return_value = mock_ds_pipeline

        result = ckihook.get_failed_stage(mock_gl, mock_project, 123)
        self.assertEqual(result, None)

        # A failed job.
        job4 = mock.Mock(status='failed', stage='prepare')
        mock_ds_pipeline.jobs.list.return_value = [job1, job2, job3, job4]

        result = ckihook.get_failed_stage(mock_gl, mock_project, 123)
        self.assertEqual(result, 'prepare')

    @mock.patch('webhook.ckihook.get_failed_stage')
    def test_get_pipeline_status(self, mock_failed_stage):
        mock_gl = mock.Mock()
        mock_project = mock.Mock()

        # No pipeline.
        pipeline = None

        (status, stage) = ckihook.get_pipeline_status(mock_gl, mock_project, pipeline)
        self.assertEqual(status, ckihook.Status.Missing)
        self.assertEqual(stage, None)

        # MR has a pipeline from some bizarre namespace.
        pipeline = {'web_url': 'https://mycoolsite.biz'}

        (status, stage) = ckihook.get_pipeline_status(mock_gl, mock_project, pipeline)
        self.assertEqual(status, ckihook.Status.Unknown)
        self.assertEqual(stage, None)

        # Pipeline has a 'good' status.
        pipeline = {'web_url': 'https://gitlab.com/redhat/project/-/pipelines/1',
                    'status': 'pending'
                    }

        (status, stage) = ckihook.get_pipeline_status(mock_gl, mock_project, pipeline)
        self.assertEqual(status, ckihook.Status.Running)
        self.assertEqual(stage, None)
        mock_failed_stage.assert_not_called()

        # Pipeline has failed, returns stage.
        pipeline['status'] = 'failed'
        pipeline['id'] = 123
        mock_failed_stage.return_value = 'test'

        (status, stage) = ckihook.get_pipeline_status(mock_gl, mock_project, pipeline)
        self.assertEqual(status, ckihook.Status.Failed)
        self.assertEqual(stage, 'test')
        mock_failed_stage.assert_called_with(mock_gl, mock_project, 123)

    @mock.patch('webhook.common.add_label_to_merge_request')
    def test_add_pipeline_label_to_mr(self, mock_add_label):
        mock_gl = mock.Mock()
        mock_project = mock.Mock()

        for status in ckihook.Status:
            stage = 'test' if status.name == 'Failed' else None
            expected = ckihook.label_string(status, stage)

            ckihook.add_pipeline_label_to_mr(mock_gl, mock_project, 123, status, stage)
            self.assertEqual(expected, mock_add_label.call_args[0][3][0]['name'])

    @mock.patch('webhook.ckihook.compute_label')
    def test_process_mr(self, mock_compute):
        mock_gl = mock.Mock()
        mr_msg = copy.deepcopy(self.PAYLOAD_MR)

        # A CKI label already exists on the MR.
        mr_msg['labels'] = [{'title': 'readyForQA'},
                            {'title': 'CKI::Running'},
                            {'title': 'Signoff::OK'}
                            ]
        msg = mock.Mock(payload=mr_msg)

        ckihook.process_mr(mock_gl, msg)
        mock_compute.assert_not_called()

        # No CKI label so compute the label.
        mr_msg['labels'] = [{'title': 'readyForQA'},
                            {'title': 'Signoff::OK'}
                            ]
        msg = mock.Mock(payload=mr_msg)

        ckihook.process_mr(mock_gl, msg)
        mock_compute.assert_called()

    @mock.patch('webhook.ckihook.compute_label')
    def test_process_pipeline(self, mock_compute):
        mock_gl = mock.Mock()
        pipe_msg = copy.deepcopy(self.PAYLOAD_PIPELINE)
        msg = mock.Mock(payload=pipe_msg)

        ckihook.process_pipeline(mock_gl, msg)
        mock_compute.assert_called()

    @mock.patch('webhook.ckihook.get_pipeline_status')
    @mock.patch('webhook.ckihook.add_pipeline_label_to_mr')
    def test_compute_label(self, mock_add_label, mock_get_status):
        mock_gl = mock.Mock()
        mock_project = mock.Mock()
        mock_mr = mock.Mock()
        mock_gl.projects.get.return_value = mock_project
        mock_project.mergerequests.get.return_value = mock_mr

        # Label hasn't changed.
        mock_mr.labels = ['Bugzilla::OK', 'CKI::Running', 'CommitRefs::NeedsReview']
        mock_get_status.return_value = (ckihook.Status.Running, None)
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            ckihook.compute_label(mock_gl, 1, 123)
            self.assertIn('MR 123 CKI label has not changed.', logs.output[-1])
            mock_add_label.assert_not_called()

        # Label changed.
        mock_mr.labels = ['Bugzilla::OK', 'CKI::Running', 'CommitRefs::NeedsReview']
        mock_get_status.return_value = (ckihook.Status.Success, None)
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            ckihook.compute_label(mock_gl, 1, 123)
            self.assertIn('MR 123 CKI status is now: Success', logs.output[-1])
            mock_add_label.assert_called_with(mock_gl, mock_project, 123, ckihook.Status.Success,
                                              None)
